# README #


This is proof of concept how to logout from children windows when parent window is logged out.

*Disclaimer: The example works only in Firefox. Chrome does not provide accessing global variables in children windows in `opener` object. Internet explorer does not fill `opener` after opening new tab.*

### How to simulate it ###
1. Open `index.html` in browser.
2. Open child window by clicking on link `Open new tab` (index2.html)
3. Look at the child window (it contains text that user is logged in)
4. Go back to parent window and click on `LOGOUT` button
5. Look at the child window and notice that user is logged out
